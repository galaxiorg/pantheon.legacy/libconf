--[[
	libconf | Configuration manager library
	        | Pantheon Project (galaxiorg/pantheon)

	Filename
	  main.lua
	Author
	  daelvn (https://gitlab.com/daelvn)
	Version
		libconf-d1
	License
		MIT License (LICENSE.md)
	Documentation
	  manual* libconf:about
		* Pantheon/manual : Manual viewer
	Description
		libconf is a library that can manage configuration in 4 different languages
		(Serialized Lua, TOML, INI and JSON) in an easy way.
	Dependencies
		Pantheon/libutil? *
		Pantheon/libwww *
		Pantheon/libini *
]]--

-- Error
error = printError or error
-- Include (Try to use Pantheon's, else make a replacement)
include = include or function(library)
	local libraryPath = library:gsub(".", "/")
	local prefixes = {
		"/rom/apis/",
		"/rom/apis/command/",
		"/rom/apis/turtle/",
		"/boot/lib/",
		"/lib/",
		""
	}
	local export
	for i, prefix in ipairs(prefixes) do
		if fs.exists(prefix..libraryPath) then
			export = dofile(prefix..libraryPath)
		elseif fs.exists(prefix..libraryPath..".lua") then
			export = dofile(prefix..libraryPath..".lua")
		elseif fs.exists(prefix..libraryPath.."/main.lua") then
			export = dofile(prefix..libraryPath.."/main.lua")
		end
	end
	return export
end

-- Export
local libconf = {}

-- Imports
local toml = include "libwww.toml"
local ini  = include "libini"
local json = include "libwww.json"

-- Global API: Interface to the settings API
libconf.global = {}
-- Set a value in a certain scope
function libconf.global.set(scope, name, value)
	settings.set(("%s.%s"):format(scope, name), value)
end
-- Get a setting in a scope
function libconf.global.get(scope, name)
	return settings.get(("%s.%s"):format(scope, name))
end
-- Unset a setting
function libconf.global.unset(scope, name)
	settings.unset(("%s.%s"):format(scope, name))
end
-- Get a list of all settings (anarchic)
function libconf.global.bulkList()
	return settings.getNames()
end
-- Get a list of settings (by scopes, hierarchic)
function libconf.global.list()
	local tree = {}
	for _, name in pairs(settings.getNames()) do
		local scope = name:match("^(%w+)%.")
		local setting = name:match("%.(%w+)")
		if (not scope) or (not setting) then return false end
		if tree[scope] then
			tree[scope][setting] = settings.get(name)
		else
			tree[scope] = {}
			tree[scope][setting] = settings.get(name)
		end
	end
	return tree
end
-- Unset a scope
function libconf.global.unsetScope(scope)
	for _, name in pairs(settings.getNames()) do
		if name:match("^"..scope.."%.") then
			settings.unset(name)
		end
	end
end
-- Load a .settings file
function libconf.global.load(path)
	settings.load(path)
end
-- Save a .settings file
function libconf.global.save(path)
	settings.save(path)
end

-- Conversion API: Convert various configuration formats
libconf.cv = {}
local function lua2ini(luaTable)
	return ini.save(luaTable)
end
local function lua2toml(luaTable)
	return toml.encode(luaTable)
end
local function lua2json(luaTable)
	return json.encode(luaTable)
end
local function json2lua(jsonString)
	return json.decode(jsonString)
end
local function toml2lua(tomlString)
	return toml.parse(tomlString)
end
local function ini2lua(iniString)
	return ini.load(iniString)
end

libconf.cv.lua = {}
libconf.cv.lua.ini   = lua2ini
libconf.cv.lua.toml  = lua2toml
libconf.cv.lua.json  = lua2json

libconf.cv.toml = {}
libconf.cv.toml.lua  = toml2lua
libconf.cv.toml.ini  = function(tomlString) return lua2ini(toml2lua(tomlString))  end
libconf.cv.toml.json = function(tomlString) return lua2json(toml2lua(tomlString)) end

libconf.cv.ini = {}
libconf.cv.ini.lua   = ini2lua
libconf.cv.ini.toml  = function(iniString)  return lua2toml(ini2lua(iniString))   end
libconf.cv.ini.json  = function(iniString)  return lua2json(ini2lua(iniString))   end

libconf.cv.json = {}
libconf.cv.json.lua  = json2lua
libconf.cv.json.ini  = function(jsonString) return lua2ini(json2lua(jsonString))  end
libconf.cv.json.toml = function(jsonString) return lua2toml(json2lua(jsonString)) end

-- Configuration API: Main libconf code
libconfmt = getmetatable(libconf)
libconfmt.__index = libconf
-- Create a new configuration set
function libconf.new(file, language, scoped)
	local config  = setmetatable({data={}}, libconf)
	config.file   = file
	config.lang   = language
	config.writer = textutils.serialize
	config.reader = textutils.unserialize
	config.scoped = scoped or false
	if
	language == "toml" then config.writer = lua2toml; config.reader = toml2lua elseif
	language == "json" then config.writer = lua2json; config.reader = json2lua elseif
	language == "ini"  then config.writer = lua2ini ; config.reader = ini2lua
	end
	return config
end
-- Set a new value
function libconf:set(scope, name, value)
	if not name then return false end
	if self.scoped then
		if not scope then return false end
		if not self.data[scope] then self.data[scope] = {} end
		self.data[scope][name] = value
		return true
	else
		self.data[scope] = name -- Trick for prettifying the function arguments
		return true
	end
end
-- Return a value
function libconf:get(scope, name)
	if not name then return false end
	if self.scoped then
		if not scope then return false end
		return self.data[scope][name]
	else
		return self.data[scope]
	end
end
-- Unset a value
function libconf:unset(scope, name)
	if not name then return false end
	if self.scoped then
		if not scope then return false end
		self.data[scope][name] = nil
		return true
	else
		self.data[scope] = nil
		return true
	end
end
-- List all data
function libconf:list()
	return self.data
end
-- List a scope
function libconf:listScope(scope)
	if not scope then return false end
	return self.data[scope]
end
-- Check if a value is set
function libconf:isSet(scope, name)
	if not name then return false end
	if self.scoped then
		if not scope then return false end
		return self.data[scope][name] ~= nil
	else
		return self.data[scope] ~= nil
	end
end
-- Compare values
function libconf:compare(scope, name, value)
	if not name then return false end
	if self.scoped then
		if not scope then return false end
		return self.data[scope][name] == value
	else
		return self.data[scope] == value
	end
end
-- Merge configuration
function libconf:merge(config)
	if (self.scoped and config.scoped) then
		for Escope, Enames in pairs(config.data) do
			if not self.data[Escope] then self.data[Escope] = {} end
			self.data[Escope] = Enames
			return true
		end
	elseif ((not self.scoped) and (not config.scoped)) then
		for name, value in pairs(config.data) do
			self.data[name] = value
			return true
		end
	else
		return false
	end
end
-- Only merge data
function libconf:mergeData(config, scoped)
	if scoped then
		for Escope, Enames in pairs(config) do
			if not self.data[Escope] then self.data[Escope] = {} end
			self.data[Escope] = Enames
			return true
		end
	elseif not scoped then
		for name, value in pairs(config) do
			self.data[name] = value
			return true
		end
	else
		return false
	end
end
-- Save with default configuration
function libconf:save()
	if not fs.exists(self.file) then return false end
	local dest = fs.open(self.file, "w")
	dest.write(self.writer(self.data))
	dest.close()
	return true
end
-- Export as other language
function libconf:export(language)
	if not fs.exists(self.file) then return false end
	if
	language == "toml" then writer = lua2toml elseif
	language == "json" then writer = lua2json elseif
	language == "ini"  then writer = lua2ini  elseif
	language == "lua"  then writer = textutils.serialize
	end
	local dest = fs.open(self.file, "w")
	dest.write(writer(self.data))
	dest.close()
	return true
end
-- Load configuration in default language
-- Overrides current configuration
function libconf:load()
	if not fs.exists(self.file) then return false end
	local src = fs.open(self.file, "r")
	local retv = self.reader(src.readAll())
	src.close()
	self.data = retv
	return true
end
-- Import configuration in another language
-- Overrides current configuration
function libconf:import(language)
	if not fs.exists(self.file) then return false end
	if
	language == "toml" then reader = toml2lua elseif
	language == "json" then reader = json2lua elseif
	language == "ini"  then reader = ini2lua  elseif
	language == "lua"  then reader = textutils.unserialize
	end
	local src = fs.open(self.file, "r")
	local retv = reader(src.readAll())
	src.close()
	self.data = retv
	return true
end
-- Loads configuration and merges with current
function libconf:mergeLoad()
	if not fs.exists(self.file) then return false end
	local src = fs.open(self.file, "r")
	local retv = self.reader(src.readAll())
	src.close()
	self:mergeData(retv)
	return true
end
-- Imports configuration and merges with current
function libconf:mergeImport(language)
	if not fs.exists(self.file) then return false end
	if
	language == "toml" then reader = toml2lua elseif
	language == "json" then reader = json2lua elseif
	language == "ini"  then reader = ini2lua  elseif
	language == "lua"  then reader = textutils.unserialize
	end
	local src = fs.open(self.file, "r")
	local retv = reader(src.readAll())
	src.close()
	self:mergeData(retv)
	return true
end
-- Convert file from language to another
function libconf:convert(from, to)
	if (not from) or (not to) then return false end
	local file = fs.open(self.file, "r")
	local converted = libconf.cv[from][to](file.readAll())
	file.close()
	local file = fs.open(self.file, "w")
	file.write(converted)
	file.close()
	self.lang = to
	return true
end
return libconf
