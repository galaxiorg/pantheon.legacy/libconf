# libconf
Configuration library with several functions

---
## Capabilities
libconf can store your configuration files in serialized lua, as .INI files, as TOML or JSON. It can export and import from this languages and nicely add and remove settings from your tree.

## Global API
Interface to the native Settings API.
#### `.global.set(scope, name, value)` -> nil
Will set a value for a property in the global CC settings. libconf divides the key into scope and name. For example, `lua.autocomplete`'s scope is `lua` and the name is `autocomplete`.
#### `.global.get(scope, name)` -> any (Serializable)
Gets a value from the global settings.
#### `.global.unset(scope, name)` -> nil
Deletes a value in the configuration.
#### `.global.bulkList()` -> table (Unindexed)
Returns all the keys of the global settings (anarchic tree).
#### `.global.list()` -> table (Tree)
Returns a tree of the settings, distributed by scope.
#### `.global.unsetScope(scope)` -> nil
Deletes all settings in a scope.
#### `.global.load(path)` -> nil
Loads the global configuration from a file (contains serialized Lua).
#### `.global.save(path)` -> nil
Saves the current configuration to a file as serialized Lua.

## Conversion API
Switch from any language to another.
#### `.cv[from][to](data)` -> string / table
Will convert from a language `from` to a language `to`, using `data`. `from` and `to` must be one of `lua`, `ini`, `json` or `toml`. `data` must be a table when converting from Lua and a string when conerting from other language.

## Configuration API
Main part of libconf utilities
#### `.new(file, language, scoped)` -> table (Object)
Will create a new configuration object, that points to a file with a language. `scoped`, which is a boolean, will determine if it will split by dots or use anarchic tables.
#### `:set(scope, name, value)` -> boolean [Scoped]
#### `:set(name, value)` -> boolean [Anarchic]
Will set a value in the configutation, if it is scoped, it will require a scope, otherwise it will use the first values.
#### `:get(scope, name)` -> any (Serializable) [Scoped]
#### `:get(name)` -> any (Serializable) [Anarchic]
Returns a value from the configuration.
#### `:unset(scope, name)` -> boolean [Scoped]
#### `:unset(name)` -> boolean [Anarchic]
Unsets a value from the configuration (set to nil).
#### `:list()` -> table
Returns a table with all the scopes (if using scoped), names and values.
#### `:listScope(scope)` -> table
Returns a table with all key-value pairs in a scope.
#### `:isSet(scope, name)` -> boolean [Scoped]
#### `:isSet(name)` -> boolean [Anarchic]
Checks if a value was set.
#### `:compare(scope, name, value)` -> boolean [Scoped]
#### `:compare(name, value)` -> boolean [Anarchic]
Checks if a name (sometimes in a scope) value matches the argument `value` exactly.
#### `:merge(object)` -> boolean
Merges another configuration object into the current one.
#### `:mergeData(data, scoped)` -> boolean
Merges only the data and not other values. A second value needs to be passed that determines if the data is scoped or not.
#### `:save()` -> boolean
Saves the file with the preset language.
#### `:export(language)` -> boolean
Saves the file in another language.
#### `:load()` -> boolean
Overrides current configuration with configuration from saved file.
#### `:import(language)` -> boolean
Overrides current configuration with configuration from saved file that is in other language.
#### `:mergeLoad()` -> boolean
Simillar to `:load()`, but instead merges the data.
#### `:mergeImport(language)` -> boolean
Simillar to `:import(language)`, but merges the data.
#### `:convert(from, to)` -> boolean
Converts the file from one language to another.

## Tips
- To change the default language without converting it yet, you can set `obj.lang` to `lua`, `ini`, `json` or `toml`.
- To modify the file it loads or saves to, modify the variable `self.file`.

## License
libconf uses the latest MIT License (LICENSE.md)
Made by [daelvn](https://gitlab.com/daelvn/).
